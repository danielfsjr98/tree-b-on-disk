import java.io.*;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class ArvoreB {
    private static final int NULL = -1;
    private static final int TRUE = 1;
    private static final int FALSE = 0;
    private static final int TAMANHO_CABECALHO = 4;
    private String nomeArq;
    private No raiz;

    private int t;
    private int TAMANHO_NO_INTERNO;
    private int TAMANHO_NO_FOLHA;
    private int NUM_MAX_CHAVES;
    private int NUM_MAX_FILHOS;

    private class No {
        private int folha;
        int nChaves;
        int endereco;
        int[] chaves = new int[NUM_MAX_CHAVES];
        int[] filhos;

        No(boolean ehFolha) {
            folha = ehFolha ? TRUE : FALSE;

            if (!ehFolha()) {
                filhos = new int[NUM_MAX_FILHOS];
                Arrays.fill(filhos, NULL);
            }
        }

        boolean ehFolha() {
            return folha == TRUE;
        }

        boolean estaCheio() {
            return nChaves == NUM_MAX_CHAVES;
        }

        void imprime() {
            System.out.println("Eh folha: " + (folha == TRUE));
            System.out.println("nChaves: " + nChaves);
            System.out.print("Chaves:");

            for (int i = 0; i < nChaves; i++) {
                System.out.print(" " + chaves[i]);
            }

            System.out.println();

            if (!ehFolha()) {
                System.out.print("Endereco dos filhos:");

                for (int i = 0; i < nChaves + 1; i++) {
                    System.out.print(" " + filhos[i]);
                }

                System.out.println();
            }
        }

        @Override
        public String toString() {
            return "No{" +
                    "folha=" + folha +
                    ", nChaves=" + nChaves +
                    ", endereco=" + endereco +
                    ", chaves=" + Arrays.toString(chaves) +
                    ", filhos=" + Arrays.toString(filhos) +
                    '}';
        }
    }

    private void inicializaConstantes(int t) {
        this.t = t;
        TAMANHO_NO_INTERNO = 2 * 4 + 4 * (2 * t - 1) + 4 * (2 * t);
        TAMANHO_NO_FOLHA = 2 * 4 + 4 * (2 * t - 1);
        NUM_MAX_CHAVES = 2 * t - 1;
        NUM_MAX_FILHOS = NUM_MAX_CHAVES + 1;
    }

    public ArvoreB(String nomeArq, int t) throws IOException {
        this.nomeArq = nomeArq;
        inicializaConstantes(t);

        if (!new File(nomeArq).exists()) {
            No no = new No(true);
            no.endereco = TAMANHO_CABECALHO;
            trocaRaiz(no);
            atualizaNo(no);
        } else {
            carregaRaizNaRAM();
        }
    }

    private void trocaRaiz(No novaRaiz) throws FileNotFoundException,
            IOException {
        RandomAccessFile arq = new RandomAccessFile(nomeArq, "rw");
        this.raiz = novaRaiz;
        arq.writeInt(this.raiz.endereco);
        arq.close();
    }

    private void carregaRaizNaRAM() throws FileNotFoundException, IOException {
        RandomAccessFile arq = new RandomAccessFile(nomeArq, "r");
        this.raiz = leNo(arq.readInt());
        arq.close();
    }

    private No leNo(int endereco) throws IOException {
        RandomAccessFile arq = new RandomAccessFile(nomeArq, "r");

        if (arq.length() == 0 || endereco == NULL) {
            arq.close();
            return null;
        }

        arq.seek(endereco);
        boolean ehFolha = arq.readInt() == TRUE;
        byte[] bytes = ehFolha ? new byte[TAMANHO_NO_FOLHA - 4]
                : new byte[TAMANHO_NO_INTERNO - 4];
        arq.read(bytes);
        ByteArrayInputStream in = new ByteArrayInputStream(bytes);
        No no = new No(ehFolha);
        no.nChaves = leInt(in);
        no.endereco = endereco;

        for (int i = 0; i < no.chaves.length; i++) {
            no.chaves[i] = leInt(in);
        }

        if (!ehFolha) {
            for (int i = 0; i < no.filhos.length; i++) {
                no.filhos[i] = leInt(in);
            }
        }

        arq.close();
        return no;
    }

    private void atualizaNo(No no) throws IOException {
        int nBytes = no.ehFolha() ? TAMANHO_NO_FOLHA : TAMANHO_NO_INTERNO;
        ByteArrayOutputStream out = new ByteArrayOutputStream(nBytes);
        escreveInt(out, no.folha);
        escreveInt(out, no.nChaves);

        for (int i = 0; i < no.chaves.length; i++) {
            escreveInt(out, no.chaves[i]);
        }

        if (!no.ehFolha()) {
            for (int i = 0; i < no.filhos.length; i++) {
                escreveInt(out, no.filhos[i]);
            }
        }

        RandomAccessFile arq = new RandomAccessFile(nomeArq, "rw");
        arq.seek(no.endereco);
        arq.write(out.toByteArray());
        arq.close();
    }

    private void gravaNovoNo(No no) throws IOException {
        no.endereco = (int) new File(nomeArq).length();
        atualizaNo(no);
    }

    private int leInt(ByteArrayInputStream in) {
        byte[] bInt = new byte[4];
        in.read(bInt, 0, 4);
        return ByteBuffer.wrap(bInt).asIntBuffer().get();
    }

    private void escreveInt(ByteArrayOutputStream out, int i) {
        byte[] num = ByteBuffer.allocate(4).putInt(i).array();
        out.write(num, 0, 4);
    }

    private void inserir(int k) throws IOException {
        if (busca(k) != -1) return;
        No r = raiz;
        if (r.estaCheio()) {
            No s = new No(false);
            s.nChaves = 0;
            s.filhos[0] = r.endereco;
            gravaNovoNo(s);
            trocaRaiz(s);
            dividirFilho(s, 0, r);
            inserirNoNaoCheio(s, k);
        } else {
            inserirNoNaoCheio(r, k);
        }
    }

    private void inserirNoNaoCheio(No x, int k) throws IOException {
        int i = x.nChaves - 1;
        if (x.ehFolha()) {
            while(i >= 0 && k < x.chaves[i]) {
                x.chaves[i+1] = x.chaves[i];
                i--;
            }
            x.chaves[i+1] = k;
            x.nChaves++;
            atualizaNo(x);
        } else {
            while (i >= 0 && k < x.chaves[i]) i--;
            i++;
            if (leNo(x.filhos[i]).estaCheio()) {
                dividirFilho(x, i, leNo(x.filhos[i]));
                if (k > x.chaves[i]) i++;
            }
            inserirNoNaoCheio(leNo(x.filhos[i]), k);
        }
    }

    private void dividirFilho(No x, int i, No y) throws IOException {
        No z = new No(y.ehFolha());
        z.nChaves = t - 1;

        for (int j = 0; j < t - 1; j++) {
            z.chaves[j] = y.chaves[j + t];
        }
        if (!y.ehFolha()) {
            for (int j = 0; j < t; j++) {
                z.filhos[j] = y.filhos[j + t];
            }
        }
        y.nChaves = t - 1;
        gravaNovoNo(z);

        for (int j = x.nChaves; j >= i + 1; j--) {
            x.filhos[j + 1] = x.filhos[j];
        }
        x.filhos[i+1] = z.endereco;

        for (int j = x.nChaves - 1; j >= i; j--) {
            x.chaves[j+1] = x.chaves[j];
        }
        x.chaves[i] = y.chaves[t-1];
        x.nChaves++;
        atualizaNo(y);
        atualizaNo(x);
    }

    public int busca(int x) throws IOException {
        No noEncontrado = buscaAux(raiz, x);
        return noEncontrado == null ? -1 : noEncontrado.endereco;
    }

    private No buscaAux(No x, int k) throws IOException {
        int i = 0;
        while (i < x.nChaves && k > x.chaves[i]) i++;

        if (i < x.nChaves && k == x.chaves[i]) return x;

        return x.ehFolha() ? null : buscaAux(leNo(x.filhos[i]), k);
    }

    public static void main(String[] args) throws IOException {
        ArvoreB arvB = new ArvoreB("arvore_b.txt", 8);

        for(int i = 500; i > 0; i--) {
            arvB.inserir(i);
        }
        for(int i = 60; i < 700; i++) {
            arvB.inserir(i);
        }

        System.out.println("Busca");
        arvB.leNo(arvB.busca(213)).imprime();

        System.out.println("----------------------");
        System.out.println("Menor q 189");
        arvB.leNo(3516).imprime();

        System.out.println("----------------------");
        System.out.println("Maior que 189 e Menor que 197");
        arvB.leNo(3316).imprime();

        System.out.println("----------------------");
        System.out.println("Maior que 197 e Menor que 205");
        arvB.leNo(3248).imprime();

        System.out.println("----------------------");
        System.out.println("Maior que 205 e Menor que 213");
        arvB.leNo(3180).imprime();

        System.out.println("----------------------");
        System.out.println("Maior que 213 e Menor que 221");
        arvB.leNo(3112).imprime();

        System.out.println("----------------------");
        System.out.println("Maior que 221 e Menor que 229");
        arvB.leNo(3044).imprime();

        System.out.println("----------------------");
        System.out.println("Maior que 229 e Menor que 237");
        arvB.leNo(2976).imprime();

        System.out.println("----------------------");
        System.out.println("Maior que 237");
        arvB.leNo(2908).imprime();

        System.out.println("----------------------");
        System.out.println("Chave inexistente");
        System.out.println(arvB.busca(-20));
    }
}
